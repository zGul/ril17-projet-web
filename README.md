# RIL17-Projet-Web

Projet web avancé, application de consultation de mentions légales (voir[ sujet ]( https://drive.google.com/file/d/1OuC8PWo8qGKkiEOW9N40mtGBVTCdDzZG/view )).

Cette application à pour but de simplifier la lecture des mentions légales de
différents services disponibles en ligne. En effet dans le cadre de la nouvelle
Réglementation sur la Protection des Données les entreprises ou organisme
collectant des données personnelles doivent être plus transparent sur les
données qu'ils collectent ainsi que sur leurs utilisation.

D'après le cahier des charges fourni par le client, nous avons pu identifier
plusieurs fonctionnalités: 

* __Pour l'administrateur:__
  * Pouvoir gérer des utilisateurs, ici pouvoir les déplacer d'un département à
    l'autre.
  * Avoir un listing des mentions et le nombre de vues.
  * Avoir un historique des dernières mentions lues, avec le nom du lecteur et
    la date de lecture.
* __Pour l'utilisateur (administrateur compris):__
  * Avoir un listing des services disponibles et pouvoir consulter les mentions
    légales.
  * Un système de gestion de profil, afin de modifier ses informations
    personnelles (nom, prénom, ect...).
  * Un récapitulatif des mentions légales lues.
* __Autres fonctionnalités:__
  * Système de mise en base de donnée pour permettre la consultation
    hors-ligne.
  * Système de mise à jour lorsque l'api ce met à jour.

## Réalisation 

* Le code source du projet ce trouve dans le dossier `application` .
* Le dossier `public` contient tous les documents à rendre pour ce projet (mcd, scripts sql, ect ...), ainsi
que les feuilles de styles et librairies javascript utilisées.

## Installation

* Faire un `git clone git@gitlab.com:zGul/ril17-projet-web.git`
* Sur un serveur Wamp, il ce peut que la librairie guzzlehttp pose un problème
  de certificat ssl. Pour le résoudre, suivre les indications données
  [ ici ](https://curl.haxx.se/docs/caextract.html).
* Créer une base donnée MySql.
* Penser à modifier le fichier `Model.php` disponible [ici](https://gitlab.com/zGul/ril17-projet-web/blob/master/application/Model/Model.php), en renseignant vos identifiants de connexion à votre base de données.
* Lancer le script [ INIT.sql ]( https://gitlab.com/zGul/ril17-projet-web/blob/master/public/document/SQL/INIT.sql ), afin de créer le schéma de la base de données.
* Lancer le script [ POPULATE.sql ]( https://gitlab.com/zGul/ril17-projet-web/blob/master/public/document/SQL/POPULATE.sql ) pour peupler la base.
* Modifier le path de GOOGLE_APPLICATION_CREDITENTIALS dans `ModelService.php` ligne 189.
* Mots de passe pour la connexion d'un utilisateur `123456`.
* Utilisateur admin : `guiz`.
* Utilisateur normal: `honor`.
* Le MCD et le dictionnaire de données sont disponibles [ ici ](https://gitlab.com/zGul/ril17-projet-web/tree/master/public/document).
* Le premier lancement de la page service peut prendre plusieurs minutes, car les services sont chargés en base depuis l'api.

## Charte graphique

Nous avons fait le choix d'utiliser la librairie Bootstrap4, avec un [ thème de
couleur dark ]( https://getbootstrap.com/docs/4.1/utilities/colors/ ). Pour nos éléments javascript, nous avons utilisé la librairie [ JqueryUI ]( https://jqueryui.com/accordion/ ) avec le thème par défaut.
Des fichiers [ CSS ]( https://gitlab.com/zGul/ril17-projet-web/tree/master/public/css ) sont également fournis pour régler la mise en page.
Les wireframe sont disponibles [ici](https://gitlab.com/zGul/ril17-projet-web/tree/master/public/document/Wireframe).

## Choix des technologies

* Le langage PHP a été utilisé coté serveur (demande explicite du client).
* La librairie CSS Bootstrap a été choisi car elle permet une mise en page rapide
et un choix de couleurs par défauts relativement intéressent.
* La librairie Javascript JQueryUi a été choisi car elle est une extension de
  JQuery, qui est nécessaire à Bootstrap. De plus elle offre également une
  mise en place d'éléments dynamiques, tel que des accordéons, très rapidement.
* Nous avons utiliser Composer pour gérer nos modules PHP (autoload, guzzlehttp, ect ...).


## Conclusion et extensions possibles

Toutes les fonctionnalités identifiées dans le cahier des charges ont été
réalisé à temps, à l'exception du système de mise à jour des services lorsque l'api est
mise à jour.

__Reste à réaliser:__

* Mettre en place un système de mise à jour des mentions légales. Pour le
  moment elles sont chargées de l'API au premier démarrage et ne sont plus
  misent à jour par la suite. Un système de mise à jour est prévu (bouton ou
  autre). 

### Extensions possibles

* Mettre en place un système de statistiques, qui permettra aux utilisateurs de
  dégager des tendances des services les plus utilisés. Ce qui pourrai
  permettre par exemple d'orienter les futurs achats de logiciels.

* Permettre un système d'ajout manuel de mentions légales, afin de compléter
  celles disponibles via L'api [Tosdr](https://tosdr.org/).

* Permettre la prise en charge de plusieurs autres langues.

* Prévoir des plugins pour navigateurs afin d'automatiser la détection des
  mentions lues.
