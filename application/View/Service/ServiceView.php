<?php

class View_ServiceView{
    
    public static function renderView($services){
        
        $gestService = new Class_GestService();
        $gestService->setListeService($services);
        $listeInitiale = $gestService->getInitialService();
        
        $html = "
            <div class='container-fluid mt-3'>
                <div class='row mb-3'>
                    <div class='col-lg-12'><form class='form-inline'>";
        
        foreach($listeInitiale as $initale){
            $html.="    <btn class='m-1 btn btn-success filterService' onclick='filterService(this)'>{$initale}</btn>";
        }
        
        $html .= "      <btn class='m-1 btn btn-success filterService' onclick='filterAll(this)'>Tous</btn>
                        <div class='input-group ml-3'>
                              <input class='form-control py-2' type='search' placeholder='Rechercher' onkeyup='filterNameService(this)' id='textFilter'>
                              <span class='input-group-append'>
                                <button class='btn btn-outline-secondary' type='button'>
                                    <i class='fa fa-search'></i>
                                </button>
                              </span>
                        </div>
                    </div>         </form>     
                </div>
                <div class='row'>
                    <div class='col-lg-12'>
                        <table>";
        foreach ($services as $service) {
            $html .= "
                            <tr data-initial='{$service->getNom()[0]}' data-name='{$service->getNom()}'>
                                <td><img src='".$service->getLogoUrl()."' alt='logo indisponible' class='ultraMiniature'/></td>
                                <td>".$service->getNom()."</td>
                                <td class='text-center'><i onclick='openServiceDetail(this)' data-id='{$service->getId()}' class='fas fa-search clickable'></i></td>
                            </tr>
            ";
        }
        $html .= "
                        </table>
                    </div>
                </div>
            </div>
        ";
        
        if(isset($_GET['id'])){
            $html.="
                <script>
                    $(document).ready(function(){
                         $('i[data-id={$_GET['id']}]').trigger('click');
                    });
                </script>
            ";
            
        }
        
        return $html;
    }
    
    public static function renderDetailServiceView(Class_Service $service){
        $html = "<div class='card'>
                    <h2 class='card-header'>
                        <img src='{$service->getLogoUrl()}' class='ultraMiniature'/>
                    {$service->getNom()} - http://localhost/ril17-projet-web/Service/?id={$service->getId()}
                    </h2>
                    <div class='row p-4'>
                        <div class='col-lg-12'>";
                    foreach($service->getListePointService() as $ptService){
                        //$ptService = new Class_PointService();
                        $html .= "
                            <h4>{$ptService->getNotation()} {$ptService->getTitre()}</h4>
                            <p>{$ptService->getDescription()}</p>";
                    }
                    if(strlen($service->getCurrentUserAgreeDate())){
                        $html .= "
                            <button class='btn btn-primary' onclick='myModal.close()'>Mention déjà lue le {$service->getCurrentUserAgreeDate()}</button>";
                    }
                    else{
                        $html .= "
                            <button data-id='{$service->getId()}' onclick='readMention(this)' class='btn btn-primary'>J'ai lu et j'accepte</button>";
                    }
                    
        $html.= "       </div>
                    </div>
                 </div>";
        
        return $html;
    }
    
}