<?php
$title = "Gestionnaire de mentions légales";
$user = $_SESSION['USER'];
$userName = $user->getPrenom().' '.$user->getNom();
$navItems = $user->getNavItem($user->getRole());
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $title; ?></title>
    <link rel="stylesheet"
    	href="/ril17-projet-web/public/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
    	href="/ril17-projet-web/public/fontawesome/css/all.css">
    <link rel="stylesheet"
    	href="/ril17-projet-web/public/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet"
    	href="/ril17-projet-web/public/css/style.css">
    <link rel="stylesheet"
    	href="/ril17-projet-web/public/jBox/scss/jBox.scss">
	<script src="/ril17-projet-web/public/jquery-3.3.1.min.js"></script>
    <script src="/ril17-projet-web/public/jquery-ui/jquery-ui.min.js"></script>
    <script src="/ril17-projet-web/public/bootstrap/js/bootstrap.min.js"></script>
    <script src="/ril17-projet-web/public/js/script.js"></script>
    <script src="/ril17-projet-web/public/jBox/js/jBox.js"></script>
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="#">Gestionnaire de mentions légales</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
				<div class="navbar-nav mr-auto">
      <?php
    
foreach ($navItems as $navItem) {
        $href = $navItem['href'];
        $active = $navItem['active'];
        $libelle = $navItem['libelle'];
        echo "<a class='nav-item nav-link {$active}' href='{$href}'>{$libelle}</a>";
    }
    ?>
      </div>
				<div class="navbar-nav">
					<span class="nav-item navbar-text mr-4"><i class="fas fa-user"></i> <?php echo $userName; ?> </span>
					<a class="nav-item nav-link" href="/ril17-projet-web/User/logOut"><i
						class="fas fa-sign-out-alt"></i> Logout</a>
				</div>
			</div>
		</nav>
	</header>
	<main>
    <?php echo $viewBag['content']; ?>
    <?php 
    	if(isset($_SESSION['flashMessage'])){
    	    foreach($_SESSION['flashMessage'] as $flashMessage){
    	        echo "<div class='infoBulle {$flashMessage['class']}'>
                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        {$flashMessage['message']}
                      </div>";
    	    }
    	    unset($_SESSION['flashMessage']);
    	}
    	?>
</main>
	<footer
		class="fixed-bottom footer-classic font-small special-color-dark bg-dark text-white pt-1">
		<!-- Copyright -->
		<div class="footer-copyright text-center py-1">
			© 2018 Copyright: <span class="text-secondary">RIL17 - Sturm
				Guillaume - Schall Anthony</span>
		</div>
		<!-- Copyright -->
	</footer>
</body>
</html>
