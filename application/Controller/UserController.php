<?php

class Controller_UserController
{

    public function __construct()
    {}

    public function indexAction()
    {            
        $return['content'] = View_UserView::renderView();
        
        return $return;
    }
    
    public function logOutAction(){
        unset($_SESSION['AUTH']);
        header('Location: /ril17-projet-web/User');
    }
    
    public function updateAction(){        
        $model = new Model_ModelUser();
        $statut =$model->updateUser($_POST);       

        if($statut){
            $_SESSION['flashMessage'][] = array(
                'message'   =>  "Modification effectuée !",
                'class'   =>  "alert alert-success"
            );
        }
        else{
            $_SESSION['flashMessage'][] = array(
                "message"   =>  "La modification n'a pas pu être effectuée ..",
                "class"   =>  "alert alert-danger"
            );
        }
        
        header('Location: /ril17-projet-web/User');
    }
    
    public function updateDepartementAction(){
        $model = new Model_ModelUser();
        $model->updateDprt($_POST['idUser'], $_POST['idDprt']);        
    }
    
}