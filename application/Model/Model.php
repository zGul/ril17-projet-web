<?php

class Model_Model
{
    protected $db;
    
    public function __construct(){
        $login = 'root';
        $mdp = '';
        
        $this->db = new mysqli(ini_get('mysqli.default_host'), $login, $mdp, "gestionnairementionlegale");
        if ($this->db->connect_errno) {
            echo "Echec lors de la connexion à MySQL : " . $this->db->connect_error;
        }
        mysqli_set_charset($this->db,"utf8");
    }
}
