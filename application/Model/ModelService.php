<?php
use Google\Cloud\Translate\TranslateClient;
class Model_ModelService extends Model_Model
{    
    public function getAllServices() {        
        $request = "SELECT * FROM T_SERVICE";
        $results = $this->db->query($request);
        $return = array();
        if($results->num_rows > 0){
            while($row = $results->fetch_assoc()){
                $service = new Class_Service($row['ID_SERVICE']);
                $service->setLogoUrl($row['LOGO_URL'])
                        ->setNom($row['NOM']);
          
                $return[] = $service; 
            }
        }
        
        return $return;
    }
    
    public function getNbUserForService($id){
        $request = "SELECT COUNT(*) FROM T_USER_READ WHERE FKID_SERVICE = {$id}";
        
        $results = $this->db->query($request);
        $return = $results->fetch_row();
        
        return $return[0];       
    }

    public function readService($idService,$idUser){
        $request = "INSERT INTO T_USER_READ (DT_READ,FKID_SERVICE,FKID_USER)
                VALUES(now(),?,?)";
        $request = $this->db->prepare("INSERT INTO T_USER_READ (DT_READ,FKID_SERVICE,FKID_USER)
                                       VALUES(now(),?,?)");
        $request->bind_param("ii",$idService,$idUser);
        $request->execute();
    }
    
    public function getServiceById($id){
        $sql = "SELECT * FROM T_SERVICE WHERE ID_SERVICE = ?";
        
        if($stmt = $this->db->prepare($sql)){
            $stmt->bind_param('i', $id);
            
            $stmt->execute();
            $res = $stmt->get_result();
            if($res->num_rows > 0){
                while($row = $res->fetch_assoc()){
                    $service = new Class_Service($row['ID_SERVICE']);
                    $service->setLogoUrl($row['LOGO_URL'])
                    ->setNom($row['NOM']);
                    
                    $service->loadService();
                }
            }
        }
        return $service;
    }
    
    public function insertService($name, $logo) {        
        $request = "INSERT INTO T_SERVICE (NOM, LOGO_URL)
                    VALUES('".$name."', '".$logo."')"; 

        $getId = "SELECT LAST_INSERT_ID() FROM T_SERVICE";
        if ($this->db->query($request) === TRUE) {
            $lastId = $this->db->query($getId);
            $return = $lastId->fetch_row();
        } else {
            echo "ERROR : " . $request."<br>". $this->db->error; 
        }
        return $return[0];
    }
     
    public function getPointServiceByIdService($id){
        $request = "SELECT * FROM T_SERVICE_POINT WHERE FKID_SERVICE = {$id}";
        $results = $this->db->query($request);
        $return = array();
        if($results->num_rows > 0){
            while($row = $results->fetch_assoc()){
                $pointService = new Class_PointService($id);
                $pointService->setDescription($row['DESCRIPTION'])
                            ->setTitre($row['TITRE'])
                            ->setNotation($row['NOTATION']);
                
                $return[] = $pointService;
            }
        }
        return $return;
    }
    
    public function insertPoint($serviceId, $point){        
        $point['title'] = self::translateToFrench($point['title']);
        $point['description'] = self::translateToFrench($point['description']);
        
        $request = $this->db->prepare(" INSERT INTO T_SERVICE_POINT (TITRE, DESCRIPTION, NOTATION, FKID_SERVICE)
                                        VALUES(?, ?, ?, ?)");
        $request->bind_param("sssi",$point['title'], $point['description'], $point['point'], $serviceId);
        $request->execute();
    }

    public function countServices(){
        $request = "SELECT COUNT(*) FROM T_SERVICE"; 
        
        $results = $this->db->query($request);
        $return = $results->fetch_row();
        
        return $return[0];
    }
    
    public function countServicesReadByUser($idUser){
        $request = "SELECT COUNT(*) FROM T_USER_READ WHERE FKID_USER={$idUser}";
        
        $results = $this->db->query($request);
        $return = $results->fetch_row();
        
        return $return[0];
    }
    
    public function isReadByUser($idService){
        $request = "SELECT DT_READ FROM T_USER_READ WHERE FKID_SERVICE={$idService} AND FKID_USER={$_SESSION['USER']->getId()}";
        
        $results = $this->db->query($request);
        $return = $results->fetch_row();
        
        return $return[0];
    }
    
    public function getServiceReadByUser(){
        $idUser = $_SESSION['USER']->getId();
        
        $request = "
            SELECT 
                USER.PRENOM + ' ' + USER.NOM AS NOM_USER,
                SERVICE.NOM AS NOM_SERVICE,
                PT.DT_READ
            FROM T_USER_READ PT
                INNER JOIN T_USER USER
                    ON USER.ID_USER = PT.FKID_USER
                INNER JOIN T_SERVICE SERVICE
                    ON SERVICE.ID_SERVICE = PT.FKID_SERVICE 
            WHERE FKID_USER = {$idUser}";
        
        $results = $this->db->query($request);
        $listMentionRead = array();
        if($results->num_rows > 0){           
            while($row = $results->fetch_assoc()){
                $mentionRead = new Class_ServiceRead($row['NOM_SERVICE'], $row['NOM_USER'], $row['DT_READ']);
                
                $listMentionRead[] = $mentionRead;
            }
            
        }
        return $listMentionRead;
    }
    
    public function getAllServiceRead(){
        $request = "
            SELECT
                CONCAT(USER.PRENOM, ' ', USER.NOM) AS NOM_USER,
                SERVICE.NOM AS NOM_SERVICE,
                PT.DT_READ
            FROM T_USER_READ PT
                INNER JOIN T_USER USER
                    ON USER.ID_USER = PT.FKID_USER
                INNER JOIN T_SERVICE SERVICE
                    ON SERVICE.ID_SERVICE = PT.FKID_SERVICE
            ORDER BY DT_READ DESC";
        
        $results = $this->db->query($request);
        $listMentionRead = array();
        if($results->num_rows > 0){
            while($row = $results->fetch_assoc()){
                $mentionRead = new Class_ServiceRead($row['NOM_SERVICE'], $row['NOM_USER'], $row['DT_READ']);
                
                $listMentionRead[] = $mentionRead;
            }
            
        }
        return $listMentionRead;
    }
    
    public function translateToFrench($string){
        $translatedString = $string;        
        
        # Your Google Cloud Platform project ID
        $projectId = 'boreal-mode-222610';
        
        putenv('GOOGLE_APPLICATION_CREDENTIALS=C:\wamp64\www\ril17-projet-web\public\cesi-ril17-infal40-groupe1-dc7f792375fd.json'); 
        
        # Instantiates a client
        $translate = new TranslateClient([
            'projectId' => $projectId,
        ]);
        
        # The target language
        $target = 'fr';
        
        # Translates some text into Russian
        $translation = $translate->translate($string, [
            'target' => $target
        ]);
        
        if(strlen($translation['text'])>0){
            $translatedString = $translation['text'];
        }
            
        return $translatedString;
    }
}
